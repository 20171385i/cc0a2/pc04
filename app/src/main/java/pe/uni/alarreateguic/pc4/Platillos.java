package pe.uni.alarreateguic.pc4;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Platillos extends BaseAdapter {
    Context context;
    String[] nomPlatos;
    int[] imagenes;

    LayoutInflater inflater;

    public Platillos(Context context, String[] nomPlatos, int[] imagenes){
        this.context = context;
        this.nomPlatos = nomPlatos;
        this.imagenes = imagenes;

    }

    @Override
    public int getCount(){
        return nomPlatos.length;
    }

    @Override
    public Object getItem(int posicion){
        return null;
    }

    @Override
    public long getItemId(int posicion){
        return 0;
    }

    @Override
    public View getView(int posicion, View convertView, ViewGroup parent){
        if(inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.item_grid, null);
        }
        ImageView imageView = convertView.findViewById(R.id.image_grid);
        TextView textView = convertView.findViewById(R.id.name_item);

        imageView.setImageResource(imagenes[posicion]);
        textView.setText(nomPlatos[posicion]);

        return convertView;

    }
}
