package pe.uni.alarreateguic.pc4;

import androidx.appcompat.app.AppCompatActivity;

import android.app.LocalActivityManager;
import android.os.Bundle;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        String[] nomPlatos = {"Caldo de Gallina","Causa rellena", "Ceviche de pescado",
                "Arroz Chaufa","Pollo a la brasa","Tamales","Tallarines rojos"};
        int[] imagenes = {R.drawable.caldogallina,R.drawable.causa,R.drawable.ceviche,
                R.drawable.chaufa,R.drawable.polloalabrasa,R.drawable.tamal,R.drawable.tallarines};
    }
}

